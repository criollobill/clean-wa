angular.module('app', [
	'app.filters',
	'ui.router',
	'ui.bootstrap',
	'ngSanitize',
	'ngMessages',
    'ngMeta',
    'ui.utils.masks',
	'monospaced.elastic',
	'angulartics',
	'angulartics.google.analytics',
	'angulartics.facebook.pixel'
])
.run(($rootScope, $document, $window, $state, $transitions, ngMeta, CONFIGS) => {
	ngMeta.init();

	$rootScope.CONFIGS = CONFIGS;
	$rootScope.scroll       = {location: 0, blocked: false};
	$rootScope.current_page = '';
	
	$document.on('scroll', function() {
		$rootScope.$apply(function() {
			$rootScope.scroll.location = $window.scrollY;
		})
	});

	$transitions.onStart({}, () => {
		$rootScope.asideOpen      = false;
		$rootScope.scroll.blocked = false;
	});

	$transitions.onSuccess({}, () => {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		$rootScope.current_page   = $state.current.name.replace(/[.]/gi, '-');
	});
})
.config(($stateProvider, $urlRouterProvider, $locationProvider, $analyticsProvider)=>{
	$analyticsProvider.withBase(true);
	$analyticsProvider.virtualPageviews(true);
	
	isLocalhost(location.href) ? $locationProvider.html5Mode(false) : $locationProvider.html5Mode(true);
 	$urlRouterProvider.otherwise('/404');
	
	$stateProvider
	.state('main', {
		templateUrl: 'views/main/main.html',
	})
	.state('main.home', {
		url: '/',
		templateUrl: 'views/home/home.html',
		meta: {
			title: 'Home',
			description: ''
		}
	})
	.state('main.404', {
		url: '/404',
		templateUrl:'views/error/404.html',
		meta: {
			title: 'Página não encontrada',
			description: 'Essa página pode ter sido temporiamente desativada ou removida.'
		}
	})
})
//////////////////////////////////////////
// Controllers
//////////////////////////////////////////

//////////////////////////////////////////
// Components
//////////////////////////////////////////

//////////////////////////////////////////
// Directive
//////////////////////////////////////////
.directive('dErrSrc', () => new errSrc())

//////////////////////////////////////////
// Services
//////////////////////////////////////////
.service('Post', Post)
