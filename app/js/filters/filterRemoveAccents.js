function filterRemoveAccents(){
    return (string) => {
        const hexAccentsMap = {
            a : /[\xE0-\xE6]/g,
            A : /[\xC0-\xC6]/g,
            e : /[\xE8-\xEB]/g,
            E : /[\xC8-\xCB]/g,
            i : /[\xEC-\xEF]/g,
            I : /[\xCC-\xCF]/g,
            o : /[\xF2-\xF6]/g,
            O : /[\xD2-\xD6]/g,
            u : /[\xF9-\xFC]|ü/g,
            U : /[\xD9-\xDC]|Ü/g,
            c : /\xE7/g,
            C : /\xC7/g,
            n : /\xF1/g,
            N : /\xD1/g,
        };

        // ensure that it's a string
        string = String(string);
        for(let character in hexAccentsMap){
            let regex = hexAccentsMap[character];
            string = string.replace(regex, character);
        }
        return string;
    }
}