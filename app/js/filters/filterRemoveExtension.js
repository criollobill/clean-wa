function filterRemoveExtension(){
    return (string) => {
        const extensions = /\.jpg|\.jpeg|\.png|\.gif|\.mp4|\.ogg|\.html|\.php/gi;
        return string.replace(extensions, '');
    }
}