const PageHeader = {
    templateUrl: 'components/page-header/page-header.html',
    bindings: {
        section: '<',
        page: '<',
    },
};