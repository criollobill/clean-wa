const PageLoading = {
    templateUrl: 'components/page-loading/page-loading.html',
    bindings: {
        hide: '=',
    },
};