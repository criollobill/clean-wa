var webPage = require('webpage');
var page    = webPage.create();
var args    = require('system').args;

page.settings.userAgent = 'phantomjs';
page.settings.loadImages = false;

page.open(args[1], function(){
	window.setTimeout(function(){
		var content = page.content;
		console.log(content);
		phantom.exit();
	}, 1000)
});