<?php
require_once('_libraries/PHPMailer-master/class.phpmailer.php');

function php_mailer($data){
    $host         = 'pleskl0014.hospedagemdesites.ws';
    $username     = 'autenticacao@megasulbrutos.com.br';
    $password     = 'xZmt508%';
    $from         = 'autenticacao@megasulbrutos.com.br';
    $from_name    = 'autenticacao@megasulbrutos.com.br';
    $port         = '587';
    
    $destiny = $data['destinatario'];
    $subject = $data['assunto'];
    $content = $data['conteudo'];

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->IsHTML(true);
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls'; // ssl, tls ou vazio
    $mail->SetLanguage('br', 'PHPMailer-master/language/');

    $mail->Host     = $host;
    $mail->Username = $username;
    $mail->Password = $password;
    $mail->From     = $from;
    $mail->FromName = $from_name;
    $mail->Port     = $port;    

    $mail->charSet = 'UTF-8'; 
    $mail->AddAddress($destiny);
    $mail->Subject = utf8_decode($subject);
    $mail->Body    = $content;

    if(!$mail->Send()) {
        echo $mail->ErrorInfo;
    }

    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->ClearAddresses();
    $mail->ClearAllRecipients();
    $mail->ClearBCCs();
    $mail->ClearCustomHeaders();

    echo 'Enviado';
}

function email($destinatario, $html, $dados = array()){

	$data = array();
	$data['destinatario'] 	= $destinatario;
	$data['data'] 			= date('Y-m-d H:i:s');
	$data['conteudo'] 		= template($html, $dados);
	$data['assunto'] 		= getTitle($data['conteudo']);

	php_mailer($data);
}

function formatBrl($x){
    return number_format($x, 2, ',', '');
}

function formatCpfCnpj($v){
    if(strlen($v) == 11){
        return substr($v, 0, 3).".".substr($v, 3, 3).".".substr($v, 6, 3)."-".substr($v, 9, 2);
    }else{
        return substr($v, 0, 2).".".substr($v, 2, 3).".".substr($v, 5, 3)."/".substr($v, 8, 4)."-".substr($v, 12, 2);
    }
}

function formatPercent($str){
    $porcentagem = explode('.', $str);
    return $porcentagem[0];
}


function formatPhone($data){
    $ddd = substr($data, 0, 2);
    if( strlen($data) === 10 ){
        $initial = substr($data, 2, 4);
        $final = substr($data, 6);
    }else{
        $initial = substr($data, 3, 5);
        $final = substr($data, 7);
    }
    return "(".$ddd.") ".$initial." ". $final ;
}

function getTitle($html){
    preg_match('/\<title\>(.*)\<\/title\>/', $html, $matches);
    return $matches[1];
}

function htmlentities_($d){
    if(is_string($d)){
        return htmlentities($d);
    }
    return $d;
}

function template($template, $data = array()){
    $data = array_map('htmlentities_', $data);
    extract($data);

    ob_start();
    include($template);
    $html = ob_get_contents();
    ob_end_clean();
    return sqlescape($html);
}