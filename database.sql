﻿# Host: localhost  (Version 5.7.14)
# Date: 2017-11-20 21:15:15
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "administrators"
#

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `administrator_id` int(11) NOT NULL AUTO_INCREMENT,
  `administrator_name` char(64) DEFAULT NULL,
  `administrator_email` char(128) DEFAULT NULL,
  `administrator_password` char(128) DEFAULT NULL,
  `administrator_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
